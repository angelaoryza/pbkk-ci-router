<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>About Us</h1>

	<div id="body">
	<h1>Welcome to My Personal Website</h1>
  <p>Hi there! My name is Angela Oryza Prabowo, and this is my personal website. I'm passionate about Computer Science, and I'm excited to share my journey and experiences with you.</p>
  
  <h2>About Me</h2>
  <p>I am a programmer with 10 years of experience. Throughout my career, I have been nominated as the best programmer more than 5 times. I believe in continuous learning and strive to stay up-to-date with the latest developments in my field.</p>
  
  <h2>My Work</h2>
  <p>On this website, you'll find information about my projects, portfolio, and accomplishments. I have worked on various model building projects and have successfully won some bidding. Feel free to explore my work and get a glimpse of what I have to offer.</p>
  
  <h2>Get in Touch</h2>
  <p>I'm always open to new opportunities and collaborations. If you have any questions, would like to discuss a potential project, or simply want to connect, feel free to reach out to me. I'd love to hear from you!</p>
  
  <p>Email: angelaoryza@gmail.com<br>
  Phone: 082298295978<br>
  </p>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>